#version 120
uniform mat4 proj;
uniform mat4 modelview;
uniform mat4 texmat;

void main()
{
	gl_Position = proj * modelview * gl_Vertex;
	gl_TexCoord[0] = texmat * gl_MultiTexCoord0;
}
