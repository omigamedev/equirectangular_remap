#version 120
uniform sampler2D tex0;

void main()
{
    vec4 y = texture2D(tex0, gl_TexCoord[0].st);
    vec4 u = texture2D(tex0, gl_TexCoord[1].st);
    vec4 v = texture2D(tex0, gl_TexCoord[2].st);

    vec4 channels = vec4(y.r, u.r, v.r, 1.0);

    mat4 conversion = mat4( 1.0,  0.0,    1.402, -0.701,
                            1.0, -0.344, -0.714,  0.529,
                            1.0,  1.772,  0.0,   -0.886,
                            0, 0, 0, 0);

    gl_FragColor = channels * conversion;
}
