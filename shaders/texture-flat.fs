#version 120
uniform sampler2D tex0;
uniform mat3 rotation;

#define PI              3.1415926535897932384626433832795
#define TWO_PI          6.283185307179586476925286766559
#define ONE_ON_PI       0.31830988618379067153776752674503
#define ONE_ON_TWO_PI   0.15915494309189533576888376337251

// https://en.wikipedia.org/wiki/Spherical_coordinate_system
// lon = u = phi   [0, 2P]
// lat = v = theta [0,  P]

void main()
{
	// [0, 1] -> lat-long
	float lat = gl_TexCoord[0].y * PI;
    float lon = gl_TexCoord[0].x * TWO_PI;

	// lat-long -> vec3
	vec3 p = vec3(sin(lat)*cos(lon), sin(lat)*sin(lon), cos(lat));

	// Rotate the sphere
	p = rotation * p;

	// [vec3] -> lat-long -> [0, 1]
	lat = acos(p.z) * ONE_ON_PI;
	lon = atan(p.y, p.x) * ONE_ON_TWO_PI;

    gl_FragColor = texture2D(tex0, vec2(lon, lat));
}
