#include "stdafx.h"
#include "Shader.h"
#include "Primitive.h"
#include "ShaderManager.h"
#include "RenderTexture.h"
#include "VideoDecoder.h"

struct Cursor
{
	static glm::vec2 pos;
	glm::vec2 dragStart;
	glm::vec2 dragDelta;
	bool dragging;
	bool down;
};
glm::vec2 Cursor::pos;

volatile bool active = true;
Cursor mouseLeft;
Cursor mouseRight;
glm::vec2 camera_rot;

VideoDecoder decoder;

GLuint CreateTexture(int w, int h, GLvoid* data)
{
	GLuint tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, 0);
	return tex;
}
GLuint LoadImage(const char* filename, int* out_w = nullptr, int* out_h = nullptr)
{
	int comp, w, h;
	unsigned char* data = stbi_load(filename, &w, &h, &comp, 3);
	if (data == nullptr)
		return 0;

	GLuint tex = CreateTexture(w, h, data);

	if (out_w) *out_w = w;
	if (out_h) *out_h = h;

	return tex;
}

void window_key(GLFWwindow* window,
	int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS)
	{
		switch (key)
		{
		case GLFW_KEY_ESCAPE:
			active = false;
			break;
		default:
			break;
		}
	}
}

void window_cursor(GLFWwindow* window, double x, double y)
{
	glm::vec2 oldPos(Cursor::pos);
	Cursor::pos = glm::vec2(x, y);
	mouseLeft.dragDelta = mouseLeft.down ? Cursor::pos - mouseLeft.dragStart : glm::vec2();
	mouseRight.dragDelta = mouseRight.down ? Cursor::pos - mouseRight.dragStart : glm::vec2();
	if (mouseLeft.down)
		camera_rot += glm::radians(Cursor::pos - oldPos);
}

void window_button(GLFWwindow* window, int button, int action, int mods)
{
	switch (button)
	{
	case GLFW_MOUSE_BUTTON_LEFT:
		mouseLeft.down = (action == GLFW_PRESS);
		mouseLeft.dragStart = Cursor::pos;
		break;
	case GLFW_MOUSE_BUTTON_RIGHT:
		mouseRight.down = (action == GLFW_PRESS);
		mouseRight.dragStart = Cursor::pos;
		break;
	}
}

void window_close(GLFWwindow* window)
{
	active = false;
}

int main(int argc, char* argv[])
{
	// Init avlib
	av_register_all();
	avcodec_register_all();
	av_log_set_level(AV_LOG_QUIET);

	glfwInit();
	GLFWwindow *window = glfwCreateWindow(1024, 512, "Equirectangular Stabilizer", nullptr, nullptr);
	glfwMakeContextCurrent(window);
	glfwShowWindow(window);
	glfwSetWindowPos(window, 30, 50);
	glewInit();

	glfwSetWindowCloseCallback(window, window_close);
	glfwSetKeyCallback(window, window_key);
	glfwSetCursorPosCallback(window, window_cursor);
	glfwSetMouseButtonCallback(window, window_button);

	ShaderManager sm;
	sm.loadShader("texture-flat");

	// Init OpenGL state
	glEnable(GL_TEXTURE_2D);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	// Init globals
	glm::mat4 proj = glm::ortho<float>(-1, 1, .5, -.5, -1, 1);

	//decoder.load(R"(C:\Users\omar\Desktop\Avicii - Waiting For Love (360 Video) (2160p).mp4)");
	cv::VideoCapture video{ R"(D:\sphericam.mp4)" };
	int vw = video.get(cv::CAP_PROP_FRAME_WIDTH);
	int vh = video.get(cv::CAP_PROP_FRAME_HEIGHT);

	if (!video.isOpened())
		return -1;


	Plane plane;
	plane.create(2, 1);
	plane.material.diffuse_tex = CreateTexture(vw, vh, nullptr);
	
	auto fd = cv::xfeatures2d::SURF::create();
	auto matcher = cv::FlannBasedMatcher::create("FlannBased");
	cv::Mat frame, old_frame, gray, old_gray, descr, old_descr;
	int rl = 200;
	cv::Rect rect{ (vw - rl) / 2, (vh - rl) / 2, rl, rl };

	std::vector<cv::KeyPoint> old_kp;
	video >> old_frame;
	cv::cvtColor(old_frame(rect), old_gray, cv::COLOR_BGR2GRAY);

	fd->detectAndCompute(old_gray, cv::Mat(), old_kp, old_descr);

	while (active && !glfwWindowShouldClose(window) && video.read(frame))
	{
		//decoder.decodeFrame();
		cv::Mat gray, rgb;
		cv::cvtColor(frame(rect), gray, cv::COLOR_BGR2GRAY);

		std::vector<cv::KeyPoint> kp;
		fd->detectAndCompute(gray, cv::Mat(), kp, descr);

		cv::Mat tr = cv::estimateRigidTransform(old_gray, gray, true);
		

		/*
		std::vector<cv::DMatch> matches;
		matcher->match(old_descr, descr, matches);

		double max_dist = 0; double min_dist = 1000;

		//-- Quick calculation of max and min distances between keypoints
		for (int i = 0; i < descr.rows && i < matches.size(); i++)
		{
			double dist = matches[i].distance;
			if (dist < min_dist) min_dist = dist;
			if (dist > max_dist) max_dist = dist;
		}

		//-- Draw only "good" matches (i.e. whose distance is less than 2*min_dist,
		//-- or a small arbitary value ( 0.02 ) in the event that min_dist is very
		//-- small)
		//-- PS.- radiusMatch can also be used here.
		std::vector<cv::DMatch> good_matches;

		for (int i = 0; i < descr.rows && i < matches.size(); i++)
		{
			if (matches[i].distance <= 2 * min_dist)
			{
				good_matches.push_back(matches[i]);
			}
		}

		//-- Draw only "good" matches
		cv::Mat img_matches;
		cv::drawMatches(gray, kp, old_gray, old_kp,
			good_matches, img_matches, cv::Scalar::all(-1), cv::Scalar::all(-1),
			std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

		//-- Show detected matches
		imshow("Good Matches", img_matches);
		*/
		cv::Rect r2 = rect;
		r2.x += tr.at<double>(0, 2);
		r2.y += tr.at<double>(1, 2);

		cv::cvtColor(frame, rgb, cv::COLOR_BGR2RGB);
		cv::rectangle(rgb, rect, cv::Scalar{ 255 }, 1);
		cv::rectangle(rgb, r2  , cv::Scalar{ 0 }, 1);
		for (auto& k : kp)
		{
			//cv::drawKeypoints(gray, { k }, gray);
			cv::circle(rgb, k.pt + cv::Point2f(rect.x, rect.y), 2, cv::Scalar{ 255 }, 2);
		}
		//cv::imshow("frame", frame);
		//if (cv::waitKey(100) == 27)
		//	active = false;

		gray.copyTo(old_gray);
		frame.copyTo(old_frame);
		descr.copyTo(old_descr);
		old_kp = std::move(kp);
		
		glBindTexture(GL_TEXTURE_2D, plane.material.diffuse_tex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, vw, vh, 0, GL_RGB, GL_UNSIGNED_BYTE, rgb.data);

		glClearColor(1, 1, 1, 1);
		glClear(GL_COLOR_BUFFER_BIT);
		sm.useShader("texture-flat");
		sm.uniformMatrix4f("proj", proj);
		sm.uniformMatrix4f("modelview", glm::mat4());
		sm.uniformMatrix4f("texmat", glm::mat4());
		sm.uniformMatrix3f("rotation", glm::mat3(glm::eulerAngleZY(-camera_rot.x, camera_rot.y)));
		sm.uniform1i("tex0", TEX_DIFFUSE);
		plane.draw(GL_TRIANGLES);

		glfwSwapBuffers(window);
		glfwPollEvents();

		//std::this_thread::sleep_for(std::chrono::milliseconds(250));
		if (cv::waitKey() == 27)
			active = false;
	}

	return 0;
}
