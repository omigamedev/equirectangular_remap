#include "stdafx.h"
#include "VideoDecoder.h"


VideoDecoder::VideoDecoder()
{
	format = nullptr;

	video_decoder = nullptr;
	video_context = nullptr;
	video_stream = nullptr;
	video_convert_ctx = nullptr;
	video_frame = nullptr;
	video_stream_index = -1;
	video_tex = 0;
	rgb_size = 0;
	width = 0;
	height = 0;
}


VideoDecoder::~VideoDecoder()
{
}


bool VideoDecoder::load(const char* filename)
{
	path = filename;

	// Open file and read streams info
	if (avformat_open_input(&format, filename, nullptr, nullptr) < 0)
		return false;
	if (avformat_find_stream_info(format, nullptr) < 0)
		return false;

	// Open video stream
	video_stream_index = av_find_best_stream(format,
		AVMEDIA_TYPE_VIDEO, -1, -1, &video_decoder, 0);
	if (video_stream_index >= 0)
	{
		video_stream = format->streams[video_stream_index];
		video_context = video_stream->codec;
		if (avcodec_open2(video_context, video_decoder, nullptr) == 0)
		{
			width = video_context->width;
			width2 = width / 2;
			height = video_context->height;
			height2 = height / 2;

			// Setup video buffers
			video_frame = av_frame_alloc();
			video_frame->format = video_context->pix_fmt;
			video_frame->width = width;
			video_frame->height = height;
			av_frame_get_buffer(video_frame, 0);
			avcodec_default_get_buffer2(video_context, video_frame, AV_GET_BUFFER_FLAG_REF);

			rgb_size = width * height * 3;

			video_rgb = av_frame_alloc();
			uint8_t* rgb_data = static_cast<uint8_t*>(av_malloc(rgb_size));
			avpicture_fill((AVPicture*)video_rgb, rgb_data, AV_PIX_FMT_BGR24, width, height);

			// Setup frame colorspace converter (usually YUV -> RGB)
			video_convert_ctx = sws_getCachedContext(nullptr, width, height, video_context->pix_fmt,
				width, height, AV_PIX_FMT_RGB24, SWS_BICUBIC, NULL, NULL, NULL);

			// Create OpenGL video texture
			glGenTextures(1, &video_tex);
			glBindTexture(GL_TEXTURE_2D, video_tex);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, rgb_data);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glBindTexture(GL_TEXTURE_2D, 0);
		}
		else
		{
			printf("Unable to open the video codec.\n");
			video_stream_index = -1;
			return 0;
		}
	}
	else
	{
		printf("Video stream not found.\n");
		video_stream_index = -1;
		return 0;
	}

	if (onLoad)
		onLoad();

	return true;
}

void VideoDecoder::decodeFrame()
{
	static AVPacket packet;
	static int rewinded = 0;
	int decoded_video = 0;
	int decoded_audio = 0;

	if (!format)
		return;

	while (!decoded_video)
	{
		av_init_packet(&packet);
		av_read_frame(format, &packet);
		if (packet.stream_index == video_stream_index)
		{
			//continue;
			int ret = avcodec_decode_video2(video_context, video_frame, &decoded_video, &packet);
			if (ret <= 0 && !decoded_video && !rewinded)
			{
				if (onComplete)
					onComplete();
				rewind();
				rewinded = 1;
				break;
			}
			if (decoded_video)
			{
				rewinded = 0;

				// Convert to RGB
				sws_scale(video_convert_ctx, video_frame->data, video_frame->linesize, 0,
					video_context->height, video_rgb->data, video_rgb->linesize);

				// Upload decoded frame to texture data
				glBindTexture(GL_TEXTURE_2D, video_tex);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, video_context->width, video_context->height, 0,
					GL_RGB, GL_UNSIGNED_BYTE, video_rgb->data[0]);
			}
		}

		if (decoded_video)
			av_free_packet(&packet);
	}
}

void VideoDecoder::close()
{
	if (format)
	{
		if (video_stream_index >= 0)
		{
			sws_freeContext(video_convert_ctx);
			av_frame_free(&video_frame);
			avcodec_close(video_context);
		}
		avformat_close_input(&format);
	}
	format = nullptr;
}

void VideoDecoder::rewind()
{
	if (format)
		av_seek_frame(format, video_stream_index, 0, AVSEEK_FLAG_BACKWARD);
}
