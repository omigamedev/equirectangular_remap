#include "stdafx.h"
#include "ShaderManager.h"
#include <fstream>

ShaderManager::ShaderManager()
{
}

bool ShaderManager::loadShader(const std::string &name)
{
    Shader &shader = m_shaders[name];
    char *vs = readFile(("./shaders/" + name) + ".vs");
    char *fs = readFile(("./shaders/" + name) + ".fs");
    if(shader.isLoaded())
        shader.unload();
    bool ret = shader.load(vs, fs);
    if(!ret) 
        printf("Error loading shader %s\n", name.c_str());
    delete vs;
    delete fs;
    return ret;
}

void ShaderManager::useShader(const std::string &name)
{
    m_currentShader = &m_shaders[name];
    m_currentShader->use();
}

void ShaderManager::uniformMatrix4f(const std::string &name, const glm::mat4 &mat)
{
    glUniformMatrix4fv(m_currentShader->getUniformLocation(name), 1, GL_FALSE, &mat[0][0]);
}

void ShaderManager::uniformMatrix4fv(const std::string &name, const float *mats, int count)
{
    glUniformMatrix4fv(m_currentShader->getUniformLocation(name), count, GL_FALSE, mats);
}

void ShaderManager::uniformMatrix3f(const std::string &name, const glm::mat3 &mat)
{
    glUniformMatrix3fv(m_currentShader->getUniformLocation(name), 1, GL_FALSE, &mat[0][0]);
}

void ShaderManager::uniform1i(const std::string &name, GLuint n)
{
    glUniform1i(m_currentShader->getUniformLocation(name), n);
}

void ShaderManager::uniform2i(const std::string &name, const glm::ivec2 &v)
{
    glUniform2i(m_currentShader->getUniformLocation(name), v.x, v.y);
}

void ShaderManager::uniform3i(const std::string &name, const glm::ivec3 &v)
{
    glUniform3i(m_currentShader->getUniformLocation(name), v.x, v.y, v.z);
}

void ShaderManager::uniform4i(const std::string &name, const glm::ivec4 &v)
{
    glUniform4i(m_currentShader->getUniformLocation(name), v.x, v.y, v.z, v.w);
}

void ShaderManager::uniform1f(const std::string &name, GLfloat f)
{
    glUniform1f(m_currentShader->getUniformLocation(name), f);
}

void ShaderManager::uniform2f(const std::string &name, const glm::vec2 &v)
{
    glUniform2f(m_currentShader->getUniformLocation(name), v.x, v.y);
}

void ShaderManager::uniform3f(const std::string &name, const glm::vec3 &v)
{
    glUniform3f(m_currentShader->getUniformLocation(name), v.x, v.y, v.z);
}

void ShaderManager::uniform4f(const std::string &name, const glm::vec4 &v)
{
    glUniform4f(m_currentShader->getUniformLocation(name), v.x, v.y, v.z, v.w);
}

char* ShaderManager::readFile(const std::string &filename, long *length)
{
    std::ifstream f(filename, std::ios_base::in | std::ios_base::ate);
    long len = (long)f.tellg();
    f.seekg(0, std::ios_base::beg);
    f.clear();
    char* buffer = new char[len+1];
    memset(buffer, 0, len+1);
    f.read(buffer, len);
    f.close();
    if(length) *length = len;
    return buffer;
}
