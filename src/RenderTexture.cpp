#include "stdafx.h"
#include "RenderTexture.h"

RenderTexture::RenderTexture()
{
    fboID = 0;
    rboID = 0;
    w = 0;
    h = 0;
}

RenderTexture::~RenderTexture()
{
    destroy();
}

void RenderTexture::destroy()
{
    if (rboID)
    {
        glDeleteRenderbuffers(1, &rboID);
    }
    if (texID)
    {
        unbindTexture();
        glDeleteTextures(1, &texID);
    }
    if (fboID)
    {
        unbindFramebuffer();
        glDeleteFramebuffers(1, &fboID);
    }
    fboID = 0;
    rboID = 0;
    w = 0;
    h = 0;
}

bool RenderTexture::create(int width, int height)
{
    // Destroy any previously created object
    destroy();

    w = width;
    h = height;

    glGenTextures(1, &texID);
    glBindTexture(GL_TEXTURE_2D, texID);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    // Create a renderbuffer object to store depth info
    glGenRenderbuffers(1, &rboID);
    glBindRenderbuffer(GL_RENDERBUFFER, rboID);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    // Create a framebuffer object
    glGenFramebuffers(1, &fboID);
    glBindFramebuffer(GL_FRAMEBUFFER, fboID);

    // Attach the texture to FBO color attachment point
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fboID, 0);

    // Attach the renderbuffer to depth attachment point
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboID);

    // Check FBO status
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE)
        printf("createColorBuffer failed\n");

    // Switch back to window-system-provided framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return status == GL_FRAMEBUFFER_COMPLETE;
}

void RenderTexture::bindFramebuffer()
{
    glBindFramebuffer(GL_FRAMEBUFFER, fboID);
}

void RenderTexture::unbindFramebuffer()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void RenderTexture::clear(glm::vec4 color)
{
    glClearColor(color.r, color.g, color.b, color.a);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void RenderTexture::bindTexture()
{
    glBindTexture(GL_TEXTURE_2D, texID);
}

void RenderTexture::unbindTexture()
{
    glBindTexture(GL_TEXTURE_2D, 0);
}
