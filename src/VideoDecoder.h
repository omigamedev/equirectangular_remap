#pragma once

#define VP_IGNORE_AUDIO false
#define VP_PBOS 16

class VideoDecoder
{
public:
	std::string      path;
	AVFormatContext* format;

	AVCodec*        video_decoder;
	AVCodecContext* video_context;
	AVStream*       video_stream;
	SwsContext*     video_convert_ctx;
	AVFrame*        video_frame;
	AVFrame*        video_rgb;
	int             video_stream_index;
	GLuint          video_tex;
	int rgb_size;
	int width;
	int height;
	int width2;
	int height2;

	VideoDecoder();
	~VideoDecoder();

	bool load(const char* filename);
	void decodeFrame();
	void rewind();
	void close();

	std::function<void(void)> onComplete;
	std::function<void(void)> onLoad;
};

