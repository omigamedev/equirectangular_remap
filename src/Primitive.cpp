#include "stdafx.h"
#include "Primitive.h"
#include "Shader.h"
#include <math.h>
#include <cmath>

BaseElement::BaseElement()
{
    ibuf = 0;
    vbuf = 0;
    i_count = 0;
    p_offset = 0;
    t_offset = 0;
    n_offset = 0;
    v_stride = 0;
    color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
}

void BaseElement::draw(GLenum type) const
{
    glActiveTexture(TEX_DIFFUSE);
    glBindTexture(GL_TEXTURE_2D, material.diffuse_tex);
    
    glActiveTexture(TEX_NORMAL);
    glBindTexture(GL_TEXTURE_2D, material.normal_tex);
    
    glActiveTexture(TEX_SPECULAR);
    glBindTexture(GL_TEXTURE_2D, material.specular_tex);
    
    glActiveTexture(TEX_SHADOW);
    glBindTexture(GL_TEXTURE_2D, material.shadow_tex);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbuf);
    glVertexPointer(3, GL_FLOAT, v_stride, (void*)p_offset);
    glNormalPointer(GL_FLOAT, v_stride, (void*)n_offset);
    
    glClientActiveTexture(GL_TEXTURE0);
    glTexCoordPointer(2, GL_FLOAT, v_stride, (void*)t_offset);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibuf);
    glDrawElements(type, i_count, GL_UNSIGNED_INT, 0);
}

void Plane::create(GLfloat w, GLfloat h)
{
    glm::vec3 pos[4], nor[4], tan[4];
    glm::vec2 tex[4];
    GLuint idx[6];
    w *= 0.5f;
    h *= 0.5f;
    GLfloat z = 0.0f;
    pos[0] = glm::vec3(-w, -h, z);
    pos[1] = glm::vec3(w, -h, z);
    pos[2] = glm::vec3(w, h, z);
    pos[3] = glm::vec3(-w, h, z);
    tex[0] = glm::vec2(0, 0);
    tex[1] = glm::vec2(1, 0);
    tex[2] = glm::vec2(1, 1);
    tex[3] = glm::vec2(0, 1);
    nor[0] = glm::vec3(0, 0, 1);
    nor[1] = glm::vec3(0, 0, 1);
    nor[2] = glm::vec3(0, 0, 1);
    nor[3] = glm::vec3(0, 0, 1);
    tan[0] = glm::vec3(0, 0, 1);
    tan[1] = glm::vec3(0, 0, 1);
    tan[2] = glm::vec3(0, 0, 1);
    tan[3] = glm::vec3(0, 0, 1);

    GLuint indexes[] = { 0, 1, 2, 2, 3, 0 };
    memcpy(idx, indexes, sizeof(idx));
    
    long i_size = sizeof(idx);
    long p_size = sizeof(pos);
    long n_size = sizeof(nor);
    long tg_size = sizeof(tan);
    long t_size = sizeof(tex);

    glGenBuffers(1, &ibuf);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibuf);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, i_size, &idx[0], GL_STATIC_DRAW);
    
    i_count = 6;
    p_offset = 0;
    n_offset = p_size;
    tg_offset = p_size + n_size;
    t_offset = p_size + n_size + tg_size;
    glGenBuffers(1, &vbuf);
    glBindBuffer(GL_ARRAY_BUFFER, vbuf);
    glBufferData(GL_ARRAY_BUFFER, p_size+n_size+t_size+tg_size, NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, p_offset, p_size, &pos[0]);
    glBufferSubData(GL_ARRAY_BUFFER, n_offset, n_size, &nor[0]);
    glBufferSubData(GL_ARRAY_BUFFER, tg_offset, tg_size, &tan[0]);
    glBufferSubData(GL_ARRAY_BUFFER, t_offset, t_size, &tex[0]);
}

Material::Material()
{
    diffuse_tex = 0;
    normal_tex = 0;
    specular_tex = 0;
    shadow_tex = 0;
}

void Sphere::create(float radius, int rings, int sectors)
{
    std::vector<GLfloat> vertices;
    std::vector<GLfloat> normals;
    std::vector<GLfloat> texcoords;
    std::vector<GLuint> indices;

    float const R = 1.f / (float)(rings-1);
    float const S = 1.f / (float)(sectors-1);
    int r, s;
    
    vertices.resize(rings * sectors * 3);
    normals.resize(rings * sectors * 3);
    texcoords.resize(rings * sectors * 2);
    std::vector<GLfloat>::iterator v = vertices.begin();
    std::vector<GLfloat>::iterator n = normals.begin();
    std::vector<GLfloat>::iterator t = texcoords.begin();
    for(r = 0; r < rings; r++) for(s = 0; s < sectors; s++) {
        float const y = (float)sin( -M_PI_2 + M_PI * r * R );
        float const x = (float)cos(2*M_PI * s * S) * (float)sin( M_PI * r * R );
        float const z = (float)sin(2*M_PI * s * S) * (float)sin( M_PI * r * R );
        
        *t++ = s*S;
        *t++ = r*R;
        
        *v++ = x * radius;
        *v++ = y * radius;
        *v++ = z * radius;
        
        *n++ = x;
        *n++ = y;
        *n++ = z;
    }
    
    indices.resize(rings * sectors * 4);
    std::vector<GLuint>::iterator i = indices.begin();
    for(r = 0; r < rings-1; r++) for(s = 0; s < sectors-1; s++) {
        *i++ = r * sectors + s;
        *i++ = r * sectors + (s+1);
        *i++ = (r+1) * sectors + (s+1);
        *i++ = (r+1) * sectors + s;
    }
    
    int i_size = (int)indices.size() * sizeof(unsigned int);
    int p_size = (int)vertices.size() * sizeof(float);
    int n_size = (int)normals.size() * sizeof(float);
    int t_size = (int)texcoords.size() * sizeof(float);
    
    glGenBuffers(1, &ibuf);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibuf);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, i_size, indices.data(), GL_STATIC_DRAW);
    
    i_count = (int)indices.size();
    p_offset = 0;
    n_offset = p_offset + p_size;
    t_offset = n_offset + n_size;
    int tot_size = t_offset + t_size;
    glGenBuffers(1, &vbuf);
    glBindBuffer(GL_ARRAY_BUFFER, vbuf);
    glBufferData(GL_ARRAY_BUFFER, tot_size, NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, p_offset, p_size, vertices.data());
    glBufferSubData(GL_ARRAY_BUFFER, n_offset, n_size, normals.data());
    glBufferSubData(GL_ARRAY_BUFFER, t_offset, t_size, texcoords.data());
}

void Sphere::draw(GLenum type)
{
    BaseElement::draw(type);
}

void DistortionMesh::create(std::vector<float> vertex, std::vector<float> texR, 
    std::vector<float> texG, std::vector<float> texB, std::vector<GLuint> index, std::vector<float> vignette)
{
    i_count = index.size();

    long i_size = i_count * sizeof(GLuint);
    long p_size = vertex.size() * sizeof(float);
    long t_size = texR.size() * sizeof(float);
    long vgn_size = vignette.size() * sizeof(float);

    p_offset = 0;
    r_offset = p_offset + p_size;
    g_offset = r_offset + t_size;
    b_offset = g_offset + t_size;
    vgn_offset = b_offset + t_size;

    glGenBuffers(1, &ibuf);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibuf);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, i_size, index.data(), GL_STATIC_DRAW);

    glGenBuffers(1, &vbuf); 
    glBindBuffer(GL_ARRAY_BUFFER, vbuf);
    glBufferData(GL_ARRAY_BUFFER, p_size + t_size * 3 + vgn_size, NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, p_offset, p_size, vertex.data());
    glBufferSubData(GL_ARRAY_BUFFER, r_offset, t_size, texR.data());
    glBufferSubData(GL_ARRAY_BUFFER, g_offset, t_size, texG.data());
    glBufferSubData(GL_ARRAY_BUFFER, b_offset, t_size, texB.data());
    glBufferSubData(GL_ARRAY_BUFFER, vgn_offset, vgn_size, vignette.data());
}

void DistortionMesh::draw(GLenum type) const
{
    glActiveTexture(TEX_DIFFUSE);
    glBindTexture(GL_TEXTURE_2D, material.diffuse_tex);

    glBindBuffer(GL_ARRAY_BUFFER, vbuf);
    glVertexPointer(3, GL_FLOAT, 0, (void*)p_offset);

    glClientActiveTexture(GL_TEXTURE0);
    glTexCoordPointer(2, GL_FLOAT, 0, (void*)r_offset);

    glClientActiveTexture(GL_TEXTURE1);
    glTexCoordPointer(2, GL_FLOAT, 0, (void*)g_offset);

    glClientActiveTexture(GL_TEXTURE2);
    glTexCoordPointer(2, GL_FLOAT, 0, (void*)b_offset);

    glEnableVertexAttribArray(ATT_VIGNETTE);
    glVertexAttribPointer(ATT_VIGNETTE, 1, GL_FLOAT, GL_FALSE, 0, (void*)vgn_offset);

    glEnableVertexAttribArray(ATT_TEXR);
    glVertexAttribPointer(ATT_TEXR, 2, GL_FLOAT, GL_FALSE, 0, (void*)r_offset);

    glEnableVertexAttribArray(ATT_TEXG);
    glVertexAttribPointer(ATT_TEXG, 2, GL_FLOAT, GL_FALSE, 0, (void*)g_offset);

    glEnableVertexAttribArray(ATT_TEXB);
    glVertexAttribPointer(ATT_TEXB, 2, GL_FLOAT, GL_FALSE, 0, (void*)b_offset);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibuf);
    glDrawElements(type, i_count, GL_UNSIGNED_INT, 0);
    glDisableVertexAttribArray(ATT_VIGNETTE);
}
